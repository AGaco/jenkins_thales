FROM openjdk:8-jre-alpine
RUN mkdir /data/
WORKDIR /data/
ADD ./target/simple*.jar ./app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
